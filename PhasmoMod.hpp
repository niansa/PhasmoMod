#ifndef _PHASMO_MOD_HPP
#define _PHASMO_MOD_HPP
#include <fstream>
#include <filesystem>
#include <vector>
#include <string>
#include <string_view>
#include <stdexcept>
#include <cstdio>
#include <cstring>
#include <cerrno>



namespace PhasmoMod {
struct Patch {
    std::string function;
    off_t offset;
    enum DataType {HEX, ASM} dataType;
    std::string data;
};

struct Mod {
    std::string name, description, notes;
    std::vector<Patch> patches;
    bool enabled = false;
};
struct Function {
    intptr_t addr;
    std::string name,
                signature;
};
using Variable = Function;


class Modder {
public:
    struct FileOpenFailed : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };
    struct FunctionNotFound : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };
    struct AssemblerFailed : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    std::filesystem::path gameAsmP;
    std::vector<Mod> mods;
    std::vector<Function> functions;
    std::vector<Variable> globalVariables;

    Modder() {}
    Modder(const std::filesystem::path& gameAsmP, std::string_view scriptJsonP)
          : gameAsmP(gameAsmP) {
        loadScriptFile(scriptJsonP);
    }

    static bool isGoodStr(std::string_view str);
    std::vector<char> hexStrToCharVec(std::string_view str);
    std::vector<char> asmStrToCharVec(const std::string& str);
    std::string charVecToHexStr(const std::vector<char>& data);
    void loadScriptFile(std::string_view scriptJsonP);
    void loadModsFile(std::string_view modsJsonP);
    void apply();
};
}
#endif
