# PhasmoMod

Simple Phasmophobia patching tool, should also work with all other il2cpp games.

Requires `rizin` to be in `$PATH`. You can also extract a rizin executable from Cutters AppImages using `--appimage-extract`: https://cutter.re/
