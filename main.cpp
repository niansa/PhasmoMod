#include "PhasmoMod.hpp"
#include "ProcPipe.hpp"

#include <iostream>
#include <cstring>



void printUsage(const char *argv0) {
    std::cout << "Usage: " << argv0 << " <mods.json file> --modlist" << std::endl
              << "Usage: " << argv0 << " <mods.json file> <script.json file> <GameAssembly.dll file> {mod IDs}" << std::endl;
}

int main(const int argc, const char **argv) {
    if (argc < 4) {
        if (argc > 2) {
            if (strcmp(argv[2], "--modlist") == 0) {
                PhasmoMod::Modder modder;
                modder.loadModsFile(argv[1]);
                size_t idx = 0;
                std::cout << "Mods available:" << std::endl;
                for (const auto& mod : modder.mods) {
                    std::cout << "ID: " << idx++ << "\n"
                                 " - Name: " << mod.name << "\n"
                                 " - Description: " << mod.description << "\n"
                                 " - Notes: " << mod.notes << "\n\n";
                }
            } else {
                printUsage(argv[0]);
            }
        } else {
            printUsage(argv[0]);
        }
    } else {
        auto modsJsonP = argv[1],
             scriptJsonP = argv[2],
             gameAssemblyDllP = argv[3];
        auto modIDs = argv + 4;

        PhasmoMod::Modder modder(gameAssemblyDllP,
                                 scriptJsonP);
        modder.loadModsFile(modsJsonP);

        if (argc > 4) {
            for (auto modID = modIDs; *modID != nullptr; modID++) {
                auto& mod = modder.mods[std::stoull(*modID)];
                mod.enabled = true;
                std::cout << "Enabling mod " << *modID << ": " << mod.name << std::endl;
            }
        } else {
            std::cout << "Enabling no mods" << std::endl;
        }

        std::cout << "Patching..." << std::endl;
        modder.apply();
        std::cout << "Finished!" << std::endl;
    }
}
