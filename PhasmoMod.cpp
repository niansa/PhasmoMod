#include "PhasmoMod.hpp"
#include "ProcPipe.hpp"

//#include <iostream>
//#include <iomanip>
#include <fstream>
#include <sstream>
#include <initializer_list>
#include <cstdlib>
#include <simdjson.h>



namespace PhasmoMod {
bool Modder::isGoodStr(std::string_view str) {
    std::string_view goodChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$._";
    for (const char c : str) {
        if (goodChars.find_first_of(c) == goodChars.npos) {
            return false;
        }
    }
    return true;
}

std::vector<char> Modder::hexStrToCharVec(std::string_view str) {
    std::vector<char> fres;
    char hexByte[2];
    bool f = true;
    for (const char c : str) {
        if (f) {
            hexByte[0] = (c <= '9') ? c - '0' : (c & 0x7) + 9;
        } else {
            hexByte[1] = (c <= '9') ? c - '0' : (c & 0x7) + 9;
            fres.push_back((hexByte[0] << 4) + hexByte[1]);
        }
        f = !f;
    }
    return fres;
}

std::vector<char> Modder::asmStrToCharVec(const std::string& str) {
    if (str != "ret") {
        std::vector<char> asmBin;
        // Write assembly to file
        {
            // Open file
            std::ofstream asmStrF("asm.s");
            // Generate assembly head
            asmStrF << "[bits 64]\n";
            for (const auto l : {globalVariables, functions}) {
                for (const auto& itm : l) {
                    if (isGoodStr(itm.name)) {
                        asmStrF << "%define " << itm.name << ' ' << itm.addr << '\n';
                    }
                }
            }
            asmStrF << str;
        }
        // Execute assembler
        if (system("nasm asm.s -o asm.bin") != 0) {
            throw AssemblerFailed("Failed to assemble: "+str);
        }
        // Read output binary
        auto asmBinSize = std::filesystem::file_size("asm.bin");
        std::ifstream asmBinF("asm.bin", std::ios_base::binary);
        asmBin.resize(asmBinSize);
        asmBinF.read(asmBin.data(), asmBinSize);
        return asmBin;
    } else {
        return {'\xc3'};
    }
}

std::string Modder::charVecToHexStr(const std::vector<char>& data) {
    std::string fres;
    fres.reserve(data.size()*2);
    for (const char b : data) {
        fres.push_back("0123456789ABCDEF"[b>>4&0xF]);
        fres.push_back("0123456789ABCDEF"[b&0xF]);
    }
    return fres;
}

void Modder::loadScriptFile(std::string_view scriptJsonP) {
    simdjson::ondemand::parser parser;
    auto json = simdjson::padded_string::load(scriptJsonP);
    auto scriptJson = parser.iterate(json);
    // Get functions
    functions.clear();
    for (auto functionJson : scriptJson["ScriptMethod"]) {
        Function fnc;
        fnc.addr = functionJson["Address"];
        fnc.name = std::string_view(functionJson["Name"]);
        fnc.signature = std::string_view(functionJson["Signature"]);
        functions.push_back(std::move(fnc));
    }
    // Get global variables
    globalVariables.clear();
    for (auto variableJson : scriptJson["ScriptMetadata"]) {
        Variable fnc;
        fnc.addr = variableJson["Address"];
        fnc.name = std::string_view(variableJson["Name"]);
        try {
            fnc.signature = std::string_view(variableJson["Signature"]);
        } catch (simdjson::simdjson_error&) {}
        globalVariables.push_back(std::move(fnc));
    }
}

void Modder::loadModsFile(std::string_view modsJsonP) {
    simdjson::ondemand::parser parser;
    auto json = simdjson::padded_string::load(modsJsonP);
    auto modsJson = parser.iterate(json);
    for (auto modJson : modsJson) {
        Mod mod;
        mod.name = std::string_view(modJson["name"]);
        mod.description = std::string_view(modJson["description"]);
        mod.notes = std::string_view(modJson["notes"]);
        for (auto patchJson : modJson["patches"]) {
            Patch patch;
            patch.function = std::string_view(patchJson["function"]);
            patch.offset = patchJson["offset"];
            patch.dataType = (std::string_view(patchJson["datatype"])=="asm")?Patch::ASM:Patch::HEX;
            patch.data = std::string_view(patchJson["data"]);
            mod.patches.push_back(std::move(patch));
        }
        mods.push_back(std::move(mod));
    }
}

void Modder::apply() {
    // Care about backup file stuff...
    std::filesystem::path gameAsmOriginalP;
    {
        auto froot = gameAsmP.parent_path();
        std::string fname = gameAsmP.filename();
        gameAsmOriginalP = froot/(fname+"-original");
        if (std::filesystem::exists(gameAsmOriginalP)) {
            // Replace non-original file
            std::filesystem::copy_file(gameAsmOriginalP, gameAsmP, std::filesystem::copy_options::overwrite_existing);
        } else {
            // Create backup of original file
            std::filesystem::copy_file(gameAsmP, gameAsmOriginalP, std::filesystem::copy_options::overwrite_existing);
        }
    }
    // Open file in rizin
    ProcPipe<true, false, false> rizin("rizin", "rizin", gameAsmP.string().c_str());
    rizin.send("oo+\n");
    // Patch in each mod
    for (const auto& mod : mods) {
        // Skip diabled mods
        if (!mod.enabled) continue;
        // Apply all patches
        for (const auto& patch : mod.patches) {
            bool found_one = false;
            for (const auto& fnc : functions) {
                if (fnc.name != patch.function && fnc.signature != patch.function) continue;
                found_one = true;
                // Get raw data to be written
                std::vector<char> data;
                if (patch.dataType == Patch::ASM) {
                    data = asmStrToCharVec(patch.data);
                } else if (patch.dataType == Patch::HEX) {
                    data = hexStrToCharVec(patch.data);
                }
                // Send data to rizin
                rizin.send("s 0x180000000 + "+std::to_string(fnc.addr+patch.offset)+"\n"
                           "wx "+charVecToHexStr(data)+"\n"
                           "s 0x180000000 + "+std::to_string(fnc.addr)+"\n"
                           "pD "+std::to_string(patch.offset+data.size())+"\n");
            }
            // Check if any function was found
            if (!found_one) {
                throw FunctionNotFound("A required function could not be found: "+patch.function);
            }
        }
    }
    // Exit rizin safely
    rizin.send("q\n");
    rizin.waitExit();
}
}
